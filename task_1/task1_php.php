<?php
// php function to convert csv to json format
function csvToJson($fname) {
    // open csv file
    $fname = "task.csv";
    if (!($fp = fopen($fname, 'r'))) {
        echo("Can't open file!");
    } else echo $fp;
    
    //read csv headers
    $key = fgetcsv($fp,"1000",",");
    // parse csv rows into array
    $json = array();
        while ($row = fgetcsv($fp,"1000",",")) {
        $json[] = array_combine(
            array_map("lcfirst",$key)
            , $row);
    }
    
    
    // release file handle
    fclose($fp);    

    // encode array to json 
    return json_encode($json);
}

//php function to create output JSON file
function createJSON(){

    //set the time and timezone for name of file

    $jfile = csvToJson("task.json");
    date_default_timezone_set('Europe/Kiev');

    $jname = date("H_i_s").".json";

    //creating and opening new JSON file

    if (!($jp = fopen($jname, 'w'))) {
        echo("Can't create file!");
    } else echo $jp;

    //writing array to JSON
    
    fwrite ( $jp ,  $jfile );
    fclose($jp);
    print_r($jfile);

}

createJSON();   //calling function "createJSON"
?>


