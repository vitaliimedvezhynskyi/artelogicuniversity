<?php 
    //include the main functions file
    include "task_functions.php"; 
?>
<?php

    //include file which connect with DB
    include "connection.php"; 
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Task</title>
</head>
<body>
    <h1>Action</h1>
    <h2>Task1<h2>
    <pre>
        <b><p>Get a list of unique visitors grouped by region </p> </b> 
    <?php action1(); ?>
    </pre>
    
    <br>
    <br>

    <h2>Task2<h2>
    <pre>
        <caption>
            <b><p>Get a count of unique visitors grouped by region - </p></b> 
            <br>
                <?php action2(); ?> 
        </caption>
    </pre>
    <h2>Task3<h2>
    <pre>
    <caption>
        <b><p>Get a a count of visits for each visitor - </p></b>
        <?php action3(); ?> 
    </caption>
    </pre>

    <h2>Task4<h2>
    <pre>
    <caption>
        <b><p>Get a count of visitors for the current year - </p></b>
        <?php action4(); ?> 
    </caption>
    </pre>

    <h2>Task5<h2>
    <pre>
    <caption>
        <b><p>Get a count of visitors grouped by months in current year -</p></b> 
        <?php action5(); ?> 
    </caption>
    </pre>
   
</body>
</html>