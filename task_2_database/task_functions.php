<?php
function action1(){
    //global connection variable
    global $connection;
    //query that returns a list of unique visitors
    $query = "SELECT DISTINCT CONCAT(first_name, ' ' ,last_name) AS VISITOR, Region
    FROM Visitors 
    ORDER BY Region";

    $result = mysqli_query($connection, $query);
        while($row = mysqli_fetch_assoc($result))
            print_r($row);
        

}
?>

<?php
function action2(){
    //global connection variable
    global $connection;
    //query that returns a count of unique visitors grouped by region
    $query = "SELECT  COUNT(DISTINCT Last_Name) AS UNIQUE_VISITORS, Region
             FROM Visitors
             GROUP BY Region;";
    $result = mysqli_query($connection, $query);
        while($row = mysqli_fetch_assoc($result))
            print_r ($row);

}
?>
<?php
function action3(){
    //global connection variable
    global $connection;
    //query that returns a count of visits for each visitor
    $query = "SELECT  COUNT( Last_Name) AS Visits, CONCAT(First_Name, ' ' ,Last_Name) AS VISITOR
            FROM Visitors
             GROUP BY Last_Name
            ORDER BY First_Name";
    $result = mysqli_query($connection, $query);
        while($row = mysqli_fetch_assoc($result))
            print_r ($row);

}
?>



<?php 
function action4(){
    //global connection variable
    global $connection;
    //query that returns a count of visitors for the current year
    $query = "SELECT Year_of_visit, COUNT(CONCAT(First_name,  Last_name)) AS VISITORS
            FROM CampGroups 
            JOIN visitors ON CampGroups.visitor_ID = visitors.ID
            WHERE CampGroups.Year_of_visit = '2020'";
    $result = mysqli_query($connection, $query);
    while($row = mysqli_fetch_assoc($result))
    print_r ($row);

}
?>

<?php
function action5(){
    //global connection variable
    global $connection;
    //query that returns a count of visitors grouped by months in current year
   $query = "SELECT Month_of_visit, Year_of_visit, COUNT(CONCAT(First_name,  Last_name)) AS VISITORS
            FROM CampGroups 
            JOIN visitors ON CampGroups.visitor_ID = visitors.ID
            WHERE CampGroups.Year_of_visit = '2020'
            GROUP BY CampGroups.Month_of_visit";
    $result = mysqli_query($connection, $query);
        while($row = mysqli_fetch_assoc($result))
            print_r ($row);

}
?>