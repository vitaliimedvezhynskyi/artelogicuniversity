-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Гру 24 2020 р., 16:14
-- Версія сервера: 10.4.16-MariaDB
-- Версія PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `campstats`
--

-- --------------------------------------------------------

--
-- Структура таблиці `campgroups`
--

CREATE TABLE `campgroups` (
  `ID` int(10) NOT NULL,
  `Name` varchar(20) DEFAULT NULL,
  `Year_of_visit` varchar(20) DEFAULT NULL,
  `Month_of_visit` varchar(20) DEFAULT NULL,
  `Visitor_ID` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `campgroups`
--

INSERT INTO `campgroups` (`ID`, `Name`, `Year_of_visit`, `Month_of_visit`, `Visitor_ID`) VALUES
(1, 'School 10', '2019', 'December', 2),
(2, 'School 10', '2019', 'December', 9),
(3, 'School 10', '2019', 'December', 7),
(4, 'University', '2020', 'March', 3),
(5, 'University1', '2020', 'April', 3),
(6, 'University1', '2020', 'April', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `visitors`
--

CREATE TABLE `visitors` (
  `ID` int(10) NOT NULL,
  `First_Name` varchar(20) DEFAULT NULL,
  `Last_Name` varchar(20) DEFAULT NULL,
  `Region` enum('Western Ukraine','Eastern Ukraine','Middle Ukraine','North Ukraine','South Ukraine') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `visitors`
--

INSERT INTO `visitors` (`ID`, `First_Name`, `Last_Name`, `Region`) VALUES
(1, 'Andrey', 'Dorokh', 'Western Ukraine'),
(2, 'Oleg', 'Pavlukh', 'South Ukraine'),
(3, 'Alina', 'Pavlova', 'Eastern Ukraine'),
(4, 'Oleg', 'Sudor', 'Western Ukraine'),
(5, 'Andrey', 'Dorokh', 'Western Ukraine'),
(6, 'Andrey', 'Dorokh', 'Western Ukraine'),
(7, 'Alina', 'Pavlova', 'Eastern Ukraine'),
(8, 'Olena', 'Zakharko', 'Middle Ukraine'),
(9, 'Vitalii', 'Ilechko', 'North Ukraine'),
(10, 'Sergii', 'Melnyk', 'Eastern Ukraine');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `campgroups`
--
ALTER TABLE `campgroups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Visitor_ID` (`Visitor_ID`);

--
-- Індекси таблиці `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `campgroups`
--
ALTER TABLE `campgroups`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблиці `visitors`
--
ALTER TABLE `visitors`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `campgroups`
--
ALTER TABLE `campgroups`
  ADD CONSTRAINT `campgroups_ibfk_1` FOREIGN KEY (`Visitor_ID`) REFERENCES `visitors` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
